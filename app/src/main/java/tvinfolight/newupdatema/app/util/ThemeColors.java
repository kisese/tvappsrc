package tvinfolight.newupdatema.app.util;

/**
 * Created by Brayo on 6/9/2016.
 */
public class ThemeColors {

    //light colors
    public static String WHITE = "#FFFFFF";
    public static String LIGHT_GRAY = "#A2A2A2";
    public static String DARK_GRAY = "#6C6C6C";
    public static String PRIMARY = "#039688";

    //dark colors
    public static String DARK_DARK_GRAY = "#696969";
}
