package tvinfolight.newupdatema.app.home;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import tvinfolight.newupdatema.app.R;
import tvinfolight.newupdatema.app.prefs.ThemePrefs;
import tvinfolight.newupdatema.app.storage.UrlsDBAdapter;
import tvinfolight.newupdatema.app.util.ThemeColors;

/**
 * Created by Brayo on 4/29/2016.
 */
public class ChannelsListAdapter extends SimpleCursorAdapter {

    private final ColorGenerator generator;
    private final Typeface type;
    private final Typeface type_2;
    TextView stream_title, stream_no;
    ImageView icon_image, icon_left;
    ImageView active_image;
    RelativeLayout list_layout;
    private int color;
    private TextDrawable drawable;
    UrlsDBAdapter urlsDBAdapter;
    String no_urls;
    private Cursor count_cursor;
    private ThemePrefs themePrefs;
    private int lastPosition = -1;

    public ChannelsListAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        generator = ColorGenerator.DEFAULT; // or use DEFAUL
        urlsDBAdapter = new UrlsDBAdapter(context);
        themePrefs = new ThemePrefs(context);
        type = Typeface.createFromAsset(context.getAssets(), "Lato-Bold.ttf");
        type_2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Medium.ttf");
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        color = generator.getRandomColor();
        stream_title = (TextView) view.findViewById(R.id.stream_title);
        stream_no = (TextView) view.findViewById(R.id.stream_no);
        icon_image = (ImageView) view.findViewById(R.id.icon_image);
        active_image = (ImageView) view.findViewById(R.id.stream_active);
        icon_left = (ImageView) view.findViewById(R.id.icon_left);
        list_layout = (RelativeLayout)view.findViewById(R.id.list_layout);

        int position = cursor.getPosition();
//        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        view.startAnimation(animation);
//        lastPosition = position;

        String title_text = cursor.getString(1);
        int active = Integer.valueOf(cursor.getString(2));

        String first_char = title_text.substring(0, Math.min(title_text.length(),1)).toUpperCase();

        if(themePrefs.getTheme().equals("normal")) {
            drawable = TextDrawable.builder()
                    .buildRoundRect(first_char, Color.DKGRAY, 10);
            icon_left.setBackgroundColor(color);
        }else if(themePrefs.getTheme().equals("light")){
            drawable = TextDrawable.builder()
                    .buildRoundRect(first_char, Color.parseColor(ThemeColors.PRIMARY), 10);
            icon_left.setBackgroundColor(Color.parseColor(ThemeColors.WHITE));
            stream_title.setTextColor(Color.parseColor(ThemeColors.LIGHT_GRAY));
        }else if(themePrefs.getTheme().equals("dark")){
            drawable = TextDrawable.builder().beginConfig().textColor(Color.parseColor(ThemeColors.DARK_DARK_GRAY)).endConfig()
                    .buildRoundRect(first_char, Color.parseColor(ThemeColors.WHITE), 10);
            icon_left.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
            stream_title.setTextColor(Color.parseColor(ThemeColors.WHITE));
            list_layout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector_dark));
        }

        icon_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_tv));
        stream_title.setText(title_text);

        count_cursor = urlsDBAdapter.queryUrlNos(title_text);
        stream_no.setText(String.valueOf(count_cursor.getCount()) + " Streams");

        if (active == 1) {
            active_image.setImageDrawable(context.getResources().getDrawable(R.drawable.chevron_right));
        } else
            active_image.setImageDrawable(context.getResources().getDrawable(R.drawable.chevron_right));

        stream_title.setTypeface(type);
        stream_no.setTypeface(type_2);
    }
}
