package tvinfolight.newupdatema.app.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.PlusShare;

import tvinfolight.newupdatema.app.R;
import tvinfolight.newupdatema.app.prefs.ThemePrefs;
import tvinfolight.newupdatema.app.storage.DBOpenHelper;
import tvinfolight.newupdatema.app.storage.StreamDBAdapter;
import tvinfolight.newupdatema.app.storage.UrlsDBAdapter;
import tvinfolight.newupdatema.app.util.ActivityStatus;
import tvinfolight.newupdatema.app.util.ConnectionDetector;
import tvinfolight.newupdatema.app.util.ThemeColors;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {
    String stream_name="c465465òhhgcjfhgbjc";
    public StreamDBAdapter streamDBAdapter;
    private UrlsDBAdapter urlDbAdapter;
    private AdView mAdView;
    public Cursor cursor;
    public ChannelsListAdapter channelsListAdapter;
    DBOpenHelper helper_ob;
    public String[] from = {helper_ob.KEY_TITLE};
    public int[] to = {R.id.stream_title};
    public ListView streams_list;
    public SearchView searchView;
    private String Colors = "http://pastebin.ca/raw/2387742";
    private FetchStreamsAsync fetchStreamsAsync;
    private ConnectionDetector connectionDetector;
    long a_, b_;
    private ActivityStatus activityStatus;
  public  InterstitialAd mInterstitialAd;
    String datainfo34[]={"h","t","t","p",":","/","/","ganja","ap","ps",".alte","rvista.","o","rg"
            ,"/","","marocchin.","js","on"};
    private ThemePrefs themePrefs;
    private Toolbar toolbar;
    private CoordinatorLayout main_layout;
    GoogleApiClient google_api_client;
    GoogleApiAvailability google_api_availability;
    SignInButton signIn_btn;
    private static final int SIGN_IN_CODE = 0;
    private ConnectionResult connection_result;
    private boolean is_intent_inprogress;
    private boolean is_signInBtn_clicked;
    private int request_code;
    ProgressDialog progress_dialog;
    private Button sharebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Colors="";
        createColors();
        //URL_="h"+"t"+"t"+"p"+":"+"/"+"/"+"ganja"+"ap"+"ps"+".alte"+"rvista."+"o"+"rg"
              //  +"/"+"TVkrkOdesk/"+"spain."+"js"+"on";
        setContentView(R.layout.activity_main);
        init();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        main_layout = (CoordinatorLayout)findViewById(R.id.main_layout);
        setSupportActionBar(toolbar);

        getSupportActionBar().setSubtitle("Find something to stream");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        streamDBAdapter = new StreamDBAdapter(this);
        connectionDetector = new ConnectionDetector(this);
        urlDbAdapter = new UrlsDBAdapter(this);
        activityStatus = new ActivityStatus(this);
        themePrefs = new ThemePrefs(this);

        themePrefs.setTheme("light");

        //get the theme
        if(themePrefs.getTheme().equals("normal")){
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            main_layout.setBackgroundColor(getResources().getColor(R.color.windowBackground));
        }else if(themePrefs.getTheme().equals("light")){
            toolbar.setBackgroundColor(Color.parseColor(ThemeColors.PRIMARY));
            main_layout.setBackgroundColor(getResources().getColor(R.color.windowBackground));
        }else if(themePrefs.getTheme().equals("dark")){
            main_layout.setBackgroundColor(Color.parseColor(ThemeColors.DARK_DARK_GRAY));
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }


        // set the ad unit ID

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //  Check the LogCat to get your test device ID
                .addTestDevice("705C3D5DBC14D8EADD3B9107816BD501")
                .build();

        // Load ads into Interstitial Ads
        // mInterstitialAd.loadAd(adRequest);
        mAdView.loadAd(adRequest);

        //have cool parralax viewpage
        streams_list = (ListView) findViewById(R.id.streams_listview);
        searchView = (SearchView) findViewById(R.id.search);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        searchView.clearFocus();


        //clear the db tables if new activity
        if (activityStatus.getStatus() == false) {
            a_ = streamDBAdapter.deleteRecordS();
            b_ = urlDbAdapter.deleteRecordS();
        }

        Log.e("Delete Response", a_ + " " + b_);

        try {
            if (streamDBAdapter.getStreamRowCount() == 0) {
                //new ReadStreamsAsync(this, this).execute();
                if (connectionDetector.isConnectingToInternet()) {
                    fetchStreamsAsync = new FetchStreamsAsync(this, this);
                    //fetchStreamsAsync.networkOperation(Colors);
                    fetchStreamsAsync.networkOperation("http://ganjaapps.altervista.org/TVkrkOdesk/update.json");
                    activityStatus.setStatus(true);
                } else {
                    Toast.makeText(MainActivity.this, "Please check our data connection", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        cursor = streamDBAdapter.queryAllStreams();
        channelsListAdapter = new ChannelsListAdapter(MainActivity.this, R.layout.channels_row,
                cursor, from, to);
        streams_list.setAdapter(channelsListAdapter);

        streams_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Cursor listCursor = (Cursor) parent.getItemAtPosition(position);
                stream_name = listCursor.getString(listCursor.getColumnIndex(helper_ob.KEY_TITLE));

                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.putExtra("stream_name", stream_name);
                if (mInterstitialAd.isLoaded()) {

                    mInterstitialAd.show();
                }else{
                    startActivity(i);
                }


            }
        });

    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showResults(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        showResults(newText);
        return false;
    }

    private void showResults(String query) {

        Cursor cursor = streamDBAdapter.queryStreams(query);

        if (cursor != null) {
            streams_list.invalidateViews();
            channelsListAdapter = new ChannelsListAdapter(MainActivity.this, R.layout.channels_row,
                    cursor, from, to);
            streams_list.setAdapter(channelsListAdapter);
        }

        //    searchView.clearFocus();
    }


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        activityStatus.setStatus(false);
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        activityStatus.setStatus(false);
        super.onDestroy();
    }
    public void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {

            mInterstitialAd.show();
        }

    }
    public void init()
    {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.HugeAd));
        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed()
            {
                requestNewInterstitial();

                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.putExtra("stream_name", stream_name);
              if(stream_name.contentEquals("c465465òhhgcjfhgbjc")){}else{
                startActivity(i);}
            }
        });
        requestNewInterstitial();
    }
    private void requestNewInterstitial()
    {
        mInterstitialAd.loadAd(getAdRequest());
    }

    public AdRequest getAdRequest()
    {
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        return adRequest;
    }
    void createColors(){
        for(int x=0;x<19;x++){
            if(x==15){
                Colors=Colors+"update/";
            }
            Colors=Colors+datainfo34[x];
        }
    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_light_theme) {
//            themePrefs.setTheme("light");
//            toolbar.setBackgroundColor(Color.parseColor(ThemeColors.PRIMARY));
//            main_layout.setBackgroundColor(getResources().getColor(R.color.windowBackground));
//            Toast.makeText(MainActivity.this, "Light Theme Selected", Toast.LENGTH_LONG);
//            reloadList();
//        }else if(id == R.id.nav_dark_theme){
//            themePrefs.setTheme("dark");
//            main_layout.setBackgroundColor(Color.parseColor(ThemeColors.DARK_DARK_GRAY));
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            reloadList();
//        }else if(id == R.id.nav_normal_theme){
//            themePrefs.setTheme("normal");
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            main_layout.setBackgroundColor(getResources().getColor(R.color.windowBackground));
//            reloadList();
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public void reloadList(){
        channelsListAdapter.notifyDataSetChanged();
        cursor = streamDBAdapter.queryAllStreams();
        channelsListAdapter = new ChannelsListAdapter(MainActivity.this, R.layout.channels_row,
                cursor, from, to);
        streams_list.setAdapter(channelsListAdapter);
    }
}
