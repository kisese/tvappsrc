package tvinfolight.newupdatema.app.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Brayo on 6/9/2016.
 */
public class ThemePrefs {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "restaurant_themes";

    public ThemePrefs(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, Context.MODE_MULTI_PROCESS);
        editor = pref.edit();
    }

    public void setTheme(String theme_name) {
        editor.putString("theme", theme_name);
        editor.commit();
    }

    public String[] getThemes(){
        String[] tags = pref.getAll().keySet().toArray(new String[0]);
        Arrays.sort(tags);
        return tags;
    }

    public String getTheme(){
        String theme_name = pref.getString("theme", "normal");
        return theme_name;
    }

    public ArrayList<String> getThemesArraylist(){
        ArrayList<String> temp = new ArrayList<>();
        String[] themes = getThemes();

        for(int i = 0; i < themes.length; i++){
            temp.add(themes[i]);
        }

        return temp;
    }

    public void editValue(String key_, String value){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key_, value);
        editor.commit();
        editor.apply();
    }

    public void deleteValue(String key_){
        SharedPreferences.Editor editor = pref.edit();
        editor = pref.edit();
        editor.remove(key_);
        editor.commit();
        editor.apply();
    }
}
